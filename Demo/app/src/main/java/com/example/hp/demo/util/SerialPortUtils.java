package com.example.hp.demo.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android_serialport_api.SerialPort;

public class SerialPortUtils {
    private String path = "/dev/ttyO2";
    private int baudrate = 9600;
    public boolean serialPortClose = false;
    public SerialPortUtils(String port,int baudrate){
        this.baudrate = baudrate;
        this.path = port;
    }
    public SerialPort serialPort = null;
    public InputStream inputStream = null;
    public OutputStream outputStream = null;
    /**
     * 打开串口，获取串口类及其输入输出流
     */
    public SerialPort open(){
        try {
            serialPort = new SerialPort(new File(path),baudrate,0);
            inputStream = serialPort.getInputStream();
            outputStream = serialPort.getOutputStream();
            new ReadThread().start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return serialPort;
    }
    /**
     * 关闭串口
     */
    public void close(){
        try {
            serialPortClose = true;
            inputStream.close();
            outputStream.close();
            serialPort.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 发送数据(字节)
     */
    public void sendByte(byte data){
        byte sendData = data;
        try {
            outputStream.write(sendData);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 发送数据(字节数组)
     */
    public void sendByteArr(byte[] data){
        byte[] sendData = data;
        try {
            outputStream.write(sendData);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 发送数据(字符串)
     */
    public void sendString(String data){
        byte[] sendData = data.getBytes();
        try {
            outputStream.write(sendData);
            outputStream.write('\n');
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 接收数据
     */
    private class ReadThread extends Thread{
        @Override
        public void run() {
            super.run();
            while (!serialPortClose) {
                try {
                    Thread.sleep(80);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                byte[] buffer = new byte[64];
                int size;
                try {
                    size = inputStream.read(buffer);
                    if (size > 0) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        int size2;
                        size2 = inputStream.read(buffer, size, 64 - size);
                        if (size2 > 0) {
                            size += size2;
                            onDataReceiveListener.onDataReceive(buffer, size);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /**
     * 接收数据监听器
     */
    public static interface OnDataReceiveListener {
        public void onDataReceive(byte[] buffer,int size);
    }
    public void setOnDataReceiveListener(OnDataReceiveListener onDataReceiveListener) {
        this.onDataReceiveListener = onDataReceiveListener;
    }
    public OnDataReceiveListener onDataReceiveListener = null;

}
